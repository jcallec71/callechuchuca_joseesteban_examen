#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 09:45:28 2020

@author: jose
"""


import time

import numpy as np

import functools

from mpi4py import MPI
 

def how_many_within_range_sequential(row, minimum, maximum):   

    count = 0

    for n in row:

        if minimum <= n <= maximum:

            count += 1

    return count

 
# Complete the how_many_within_range_parallel function below.

def how_many_within_range_parallel(row, minimum, maximum):

    MASTER = 0
    FROM_MASTER = 0
    FROM_WORKER = 0
    
    dest = 0
    rows = 0
    rc = 0
    source = 0
    
    comm =  MPI.COMM_WORLD
    
    numtasks = comm.Get_size()

    taskid = comm.Get_rank()
    
    numworkers =  numtasks - 1
    
    
    if taskid == MASTER:
    
        mtype = FROM_MASTER
        
        for dest in range(numworkers):
    
            comm.send(row, dest=dest+1, tag=mtype)
            comm.send(minimum, dest=dest+1, tag=mtype)
            comm.send(maximum, dest=dest+1,tag=mtype)
            
        mtype = FROM_WORKER
        
        for n in range(numworkers):
            
            source = n
            rows = comm.recv(source=source+1, tag=mtype)
            result = comm.recv(source=source+1, tag=mtype)
            
            return result
            
    if(taskid > MASTER):
        
        mtype = FROM_MASTER
     
        rows = comm.recv(source=MASTER, tag=mtype)
        minimum = comm.recv(source=MASTER, tag=mtype)
        maximum = comm.recv(source=MASTER, tag=mtype)
        
        count = 0

        for n in rows:

            if minimum <= n <= maximum:
                
                count += 1
        
     
        comm.send(rows,dest=MASTER, tag=mtype)
        comm.send(count,dest=MASTER,tag=mtype)
        

if __name__ == '__main__':

 

    # Prepare data

    np.random.RandomState(100)

    arr = np.random.randint(0, 10, size=[4000000, 10])

    ar = arr.tolist()


    inicioSec = time.time()

    resultsSec = []

    for row in ar:

        resultsSec.append(how_many_within_range_sequential(row, minimum=4, maximum=8))

    finSec =  time.time()

   

    # You can modify this to adapt to your code
    

    inicioPar = time.time()   

    resultsPar = []

    for row in ar:

        resultsPar.append(how_many_within_range_parallel(row, minimum=4, maximum=8))

    finPar = time.time()   

   

    print('Results are correct!\n' if functools.reduce(lambda x, y : x and y, map(lambda p, q: p == q,resultsSec,resultsPar), True) else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms \n' % ((finSec - inicioSec)*1000))

    print('Parallel Process took %.3f ms \n' % ((finPar - inicioPar)*1000))