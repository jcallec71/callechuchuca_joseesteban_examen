Para el multiprocessing realize la division de ar en 8 partes y este mande al metodo para que mediante el uso de Pool se ejecutara el multiprocessing, el tiempo me sale mas que en secuencial
por lo que tendre que realizar correciones respectivas.

python CALLE_CHUCHUCA_JOSE_ESTEBAN_ExamenMultiprocessing.py

![Multiprocessing](https://gitlab.com/jcallec71/callechuchuca_joseesteban_examen/-/blob/master/img1.png)


En el proceso MPI aplique el pasar los valores de ingreso que son los rows, minimum y maximum para procesarlos dentro del if correspondiente. El error no se imprime ya que al parecer entra a
una especie de bucle que cuelga la maquina, de igual manera realizare las correciones respectivas.


mpiexec -n 4 python CALLE_CHUCHUCA_JOSE_ESTEBAN_ExamenMPI.py


