#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 09:45:28 2020

@author: jose
"""


import time

import numpy as np

import functools

import multiprocessing

 

def how_many_within_range_sequential(row, minimum, maximum):   

    count = 0

    for n in row:

        if minimum <= n <= maximum:

            count += 1

    return count

 

# Complete the how_many_within_range_parallel function below.

def how_many_within_range_parallel(ar, minimum, maximum):
    
    for row in ar:
        
        count = 0
        
        for n in row:

            if minimum <= n <= maximum:

                count += 1


    return count

 

if __name__ == '__main__':

 

    # Prepare data

    np.random.RandomState(100)

    arr = np.random.randint(0, 10, size=[4000000, 10])

    ar = arr.tolist()

   

    inicioSec = time.time()

    resultsSec = []

    for row in ar:

        resultsSec.append(how_many_within_range_sequential(row, minimum=4, maximum=8))

    finSec =  time.time()

   

    # You can modify this to adapt to your code

    inicioPar = time.time()   

    resultsPar = []
    
    ar1 = ar[0 : int(len(ar)/4)]
    ar2 = ar[int(len(ar)/8) : int(2*len(ar)/8)]
    ar3 = ar[int(2*len(ar)/8) : int(3*len(ar)/8)]
    ar4 = ar[int(3*len(ar)/8) : int(4*len(ar)/8)]
    ar5 = ar[int(4*len(ar)/8) : int(5*len(ar)/8)]
    ar6 = ar[int(5*len(ar)/8) : int(6*len(ar)/8)]
    ar7 = ar[int(6*len(ar)/8) : int(7*len(ar)/8)]
    ar8 = ar[int(7*len(ar)/8) : len(ar)]
    
    p = multiprocessing.Pool(8)
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar1, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar2, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar3, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar4, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar5, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar6, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar7, 4, 8, )))
    resultsPar.append(p.apply(how_many_within_range_parallel, (ar8, 4, 8, )))

    finPar = time.time()   
   

    print('Results are correct!\n' if functools.reduce(lambda x, y : x and y, map(lambda p, q: p == q,resultsSec,resultsPar), True) else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms \n' % ((finSec - inicioSec)*1000))

    print('Parallel Process took %.3f ms \n' % ((finPar - inicioPar)*1000))